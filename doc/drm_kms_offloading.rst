.. Copyright 2022 Advanced Micro Devices, Inc.

DRM/KMS Offloading
==================

Even though all of the transformations for a color-managed Wayland compositor
can be performed via graphics or compute shaders this might not always be the
most efficient use of HW. Most display HW offers some form of 1D LUTs and color
transformation matrixes before or after the blending stage. Some HW even
provides 3D LUTs. While all of these can be easily implemented via shaders,
doing so usually burns a lot more power than using fixed-function HW. It also
adds an additional step in the composition pipeline, which usually means
increased latency.

On desktop systems the additional power consumption and latency is usually not
a major concern when the target application isn't gaming. On mobile platforms,
like Chromebooks, power becomes a major concern. In gaming use-cases latency
is an important consideration.

(TODO edit or rewrite)

When a compositor is not using DRM/KMS composition it has the choice of using
DRM/KMS LUTs to encode the content in a non-linear wire format, or to use
shaders for this operation. This document will first talk about this before
moving on to multi-plane use cases.

Pixel Formats
-------------

When presenting HDR content we need to represent the entire dynamic range of our
content with enough quantization to represent contrast steps that remain below
the perception threshold of human eyes. For PQ encoded content, spanning the
range of 0 to 10,000 nits this requires 12-bit quantization. 10-bit quantization
is not quite enough to remain below the perception threshold, though in
practice, when taking camera noise into account, these minor quantization
artifacts are masked. See Section 5.2 of the `BT.2390 spec`_ for more details.

.. _`BT.2390 spec`: https://www.itu.int/dms_pub/itu-r/opb/rep/R-REP-BT.2390-10-2021-PDF-E.pdf

This means that non-linearly encoded content can be encoded via 10 bpc formats.
The format of choice is usually P010 or ARGB2101010.

Linearly encoded content requires something like FP16, 16 bpc, represented in
floating point.

The pixel format of a framebuffer will have an impact on the memory bandwidth in
the system. Lower memory bandwidth usually means less power consumption,
therefore choosing the right pixel format will have an impact on our power
consumption.

.. list-table::
   :header-rows: 1

   * - Pixel format
     - Bandwidth (per pixel, in bytes)
   * - P010
     - 2
   * - ARGB2101010
     - 4
   * - FP16
     - 8

Even though FP16 is a great choice for compositors to represent the blending
space it has a large impact on memory bandwidth and might not be the best choice
for mobile platforms. Representing the framebuffer content via standard
non-linear encodings and requesting display HW to linearize it prior to scaling
and blending is expected to have a benficial impact on power as as opposed to
asking for FP16 scanout of linear content.

A fully color-managed compositor might use linear formats for composition. In
this scenario performing a non-linearization of the content (presumably via
shaders) can be costly in and off itself. One needs to evaluate whether this
tradeoff is worthwhile.

Single-plane use-cases
----------------------

In the simplest case (from a DRM/KMS point of view) the compositor serves a
single, scaled, composited, non-linear plane to the DRM/KMS HW and the HW
operates in bypass, i.e. the framebuffer values are the same ones that go out
on the wire to the display. The compositor might provide a HDR_OUTPUT_METADATA
`DRM connector property`_ to provide additional information about the HDR
content to the display.

If the compositor wants to take advantage of the DRM/KMS HW to convert from the
linear luminance content of the blending space to the non-linear encoding of the
content on the wire it can present the framebuffer in a suitable format in
linear space (e.g. FP16) and program a DRM CRTC LUT (gamma or degamma)
to transform the content into the non-linear wire format expected by the
display. This use-case also allows for linear scaling in DRM/KMS HW.

The current DRM CRTC API for Gamma and Degamma LUTs uses a uniform
representation. This is not well-suited for linear inputs. A proposal to define
`non-linearly distributed PWLs`_ is under review and will likely get more
attention as the similarly defined new `DRM Plane Gamma and Degamma PWLs`_ are
fleshed out.

.. _`DRM connector property`: https://dri.freedesktop.org/docs/drm/gpu/drm-kms.html#standard-connector-properties
.. _`non-linearly distributed PWLs`: https://patchwork.freedesktop.org/series/90822/
.. _`DRM Plane Gamma and Degamma PWLs`: https://patchwork.freedesktop.org/series/90826/

Multi-plane use-cases
---------------------

(TODO needs some editing or rewriting)

Most modern display HW has the ability to blend framebuffers in HW. This is
exposed to compositors via DRM/KMS planes. A compositor can use multiple DRM/KMS
planes instead of blending all buffers via shaders.

Unfortunately we don't have an unlimited supply of DRM/KMS planes available.
On some HW these planes might be shared between multiple outputs, on other HW
DRM/KMS planes have different capabilities in terms of supported framebuffer
size, supported transformations (HDR vs SDR), abilities to scale the content,
etc. The differences can be as diverse as the HW on the market. For this reason
a compositor needs to check whether a given plane configuration is supported
before enabling it.

A compositor will usually not be able to use DRM/KMS for all of its buffers and
has to make a policy decision about which ones to offload to DRM/KMS planes and
which ones to compose via shaders. This, and the following sections of this
document, are intended to help provide some guidance to compositor developers in
making this decision.


Latency-reduction for gaming
----------------------------

In gaming use-cases it might be beneficial to separate the gaming plane from
an overlay plane or from the rest of the desktop, in order to avoid the
time it takes to perform the composition in shaders. This is mainly to reduce
latency, but also to free up a bit of GPU bandwidth for the game itself.

(Details TBD)

I imagine games would like to present their framebuffers in FP16 and avoid
the non-linearization step at the end, but this use-case definition should be
driven by how games currently present content through DirectX and other
interfaces that support HDR.

Multi-plane power-saving
------------------------

WIP

- Why do we only want to use separate planes for some use-cases?
  - makes sense when plane updates often while rest of screen doesn't
  - avoid GPU composition, ideally leaving GPU off for extended periods
  - avoid read and write of pixels by GPU composition pipelines


Another use-case is to save power when doing video playback. Video surfaces are
generally presented to the compositor in YUV (NV12 or P010) formats. Hence the
compositor needs to perform color space conversion for the video. In addition,
when video playback occurs the rest of the desktop is often mostly static, be
it because the video is in fullscreen mode, or because the user is otherwise
focussed on the (windowed) video and not interacting much with the rest of the
desktop. This means that the compositor is using the GPU for composition mainly
for the purposses of composing the video. If we use a dedicated YUV DRM/KMS
plane for the video, GPU composition can be idle for long stretches of time,
saving a lot of power.

Since composition of multiple color-managed surfaces can be challenging it makes
sense to focus on these two use-cases for DRM/KMS offloading. In the future
this set of use-cases can be expanded once we have a good foundation.






