.. Copyright 2020 Collabora, Ltd.


Glossary
========
API
   application programming interface

AVI
   auxiliary video information (infoframe)

CM
   color management

CMM
   color management module

CRTC
   cathode-ray tube controller, nowadays a hardware block or an abstraction
   that produces a timed stream of raw digital video data

DRM
   direct rendering manager

EOFT
   electro-optical transfer function

HDR
   high dynamic range

KMS
   kernel modesetting, display driver kernel-userspace API

LUT
   look-up table

nit
   cd/m², candelas per square meter, absolute unit of luminance

OETF
   opto-electrical transfer function

SDR
   standard dynamic range

VCGT
   video card gamma table, a non-standard tag added into ICC profiles
   originally by Apple

Wayland
   a window system protocol

X11
   a window system protocol
